<?php

declare(strict_types=1);

namespace PhpGuild\MediaObjectBundle\EventSubscriber\DoctrineClassMetadata;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Doctrine\ORM\Mapping\MappingException;
use PhpGuild\MediaObjectBundle\Model\File\FileInterface;

/**
 * Class FileMetadataSubscriber
 */
#[AsDoctrineListener(event: Events::loadClassMetadata, priority: 256, connection: 'default')]
class FileMetadataSubscriber
{
    /**
     * loadClassMetadata
     *
     * @param LoadClassMetadataEventArgs $loadClassMetadataEventArgs
     *
     * @throws MappingException
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $loadClassMetadataEventArgs): void
    {
        $classMetadata = $loadClassMetadataEventArgs->getClassMetadata();

        if (
            true === $classMetadata->isMappedSuperclass
            || null === $classMetadata->reflClass
            || !is_a($classMetadata->reflClass->getName(), FileInterface::class, true)
        ) {
            return;
        }

        $classMetadata->mapField([
            'nullable' => true,
            'type' => Types::STRING,
            'fieldName' => FileInterface::FILE_COLUMN_NAME,
            'columnName' => FileInterface::FILE_COLUMN_NAME,
        ]);
    }
}
